# Roseta

A spiraling typeface with an attitude. You can hear it singing if you get close enough.

Made in the [Type:Bits workshop](https://typebits.gitlab.io) in Avilés, 2017.

![Font preview](https://gitlab.com/typebits/font-roseta/-/jobs/artifacts/master/raw/Roseta-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-roseta/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-roseta/-/jobs/artifacts/master/raw/Roseta-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-roseta/-/jobs/artifacts/master/raw/Roseta-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-roseta/-/jobs/artifacts/master/raw/Roseta-Regular.sfd?job=build-font)

# Authors

This typeface was created by ESAPA students at the Type:Bits workshop in Avilés, 2017.

# License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
